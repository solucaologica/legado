<%@page import="br.com.legado.config.ServerConfig"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
 <link rel="icon" href="./../assets/img/favicon.ico" type="image/x-icon">
 <link rel="stylesheet" href="./../assets/css/bootstrap.min.css" type="text/css">
 <link rel="stylesheet" href="./../assets/css/bootstrap.min.css.map" type="text/css">
 <link rel="stylesheet" href="./../assets/css/theme.css" type="text/css">
 <title><%=ServerConfig.APP%></title>
</head>
<body>
  <!-- Imagem de plano de fundo -->
  <div class="text-white p-0 h-25" style="background-image: url(&quot;https://pingendo.github.io/templates/sections/assets/test_lily.jpg&quot;); background-size: cover;">
    <div class="container">
      <div class="row">
        <!-- Foto de perfil que ser� exibida -->
        <div class="col-md-6    ">
          <br> </div>
        <div class="col-md-6 my-4 px-4 ">
          <!-- Bot�o para alterar a imagem de plano de fundo-->
          <a class="btn btn-sm text-capitalize btn-primary" href="#">ALTERAR IMAGEM DE FUNDO
            <br> </a>
        </div>
      </div>
    </div>
  </div>
  <!-- In�cio do menu -->
  <nav class="navbar navbar-expand-md navbar-dark bg-secondary">
    <div class="container">
      <a class="navbar-brand" href="#">Projeto Stags&nbsp;
        <br> </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse text-center justify-content-end" id="navbar2SupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="home.html">
              <b> Home </b>
              <br> </a>
          </li> &nbsp;
          <li class="nav-item">
            <a class="nav-link" href="competencias.html">
              <b>Competencias </b>
              <br> </a>
          </li> &nbsp;
          <li class="nav-item">
            <a class="nav-link" href="aprendizado.html">
              <b>Treinamentos </b>
              <br> </a>
          </li> &nbsp;
          <li class="nav-item">
            <a class="nav-link" href="perfil.html">
              <b>Perfil </b>
              <br> </a>
          </li> &nbsp;
          <li class="nav-item">
            <a class="nav-link" href="sobre.html">
              <b>Sobre </b>
              <br> </a>
          </li> &nbsp; </ul>
        <a class="btn navbar-btn btn-primary ml-2 text-white">
          <i class="fa d-inline fa-lg fa-user-circle-o"></i> Sair</a>
      </div>
    </div>
  </nav>
  <!-- Imagem de perfil -->
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <img class="img-fluid d-block rounded-circle" src="https://pingendo.com/assets/photos/wireframe/photo-1.jpg">
          <br>
          <!-- Bot�o para alterar foto -->
          <button type="submit" class="btn btn-block btn-sm btn-info">
            <b>Nova Foto</b>
          </button>
          <!-- Bot�o para exibi��o de curr�culo  -->
        </div>
        <!-- In�cio do form principal -->
        <div class="col-md-6 ">
          <form class="" action="<%=ServerConfig.HOST%>/legado/user/new" method="post">
            <!-- Dados pessoais -->
            <div class="form-group">
              <h1> Dados Pessoais </h1>
              <!--Campo do Nome -->
              <label>
                <b>Nome</b>
              </label>
              <input type="email" class="form-control" placeholder="Nome">
              <small class="form-text text-muted"></small>
              <br>
              <!-- campo do sobrenome-->
              <label>
                <b>Sobrenome</b>
              </label>
              <input type="email" class="form-control" placeholder="Sobrenome">
              <br>
              <!-- Campo do CPF -->
              <label>
                <b>Cpf</b>
              </label>
              <input type="number" class="form-control" placeholder="___.___.___-__">
              <br>
              <!-- Campo do RG -->
              <label>
                <b>Rg</b>
              </label>
              <input type="number" class="form-control" placeholder="">
              <br>
              <!-- Campo do CEP -->
              <label>
                <b>Cep</b>
              </label>
              <input type="number" class="form-control" placeholder="Digite seu cep">
              <br>
              <!-- Campo da Data -->
              <label>
                <b>Data de Nascimento</b>
              </label>
              <br>
              <input type="date" name="txtBirth" placeholder="" class="form-control">
              <br>
              <br>
              <button type="submit" class="btn btn-success"> Salvar
                <br> </button>
              <br>
              <br>
              <!-- Contato -->
              <h1> Contato </h1>
              <br>
              <!-- Campo telefone cel -->
              <label>
                <b>Tel. Celular</b>
              </label>
              <br>
              <input type="number" class="form-control" placeholder="(xx)xxxxx-xxxx">
              <br>
              <!-- Campo telefonr residencial -->
              <label>
                <b>Tel. Residencial</b>
              </label>
              <input type="number" class="form-control" placeholder="(xx)xxxx-xxxx">
              <!-- Campo telefone de emergencia -->
              <br>
              <label>
                <b>Nome do Contato de Emerg�ncia</b>
              </label>
              <input type="text" class="form-control" placeholder="Informe o nome aqui">
              <br>
              <label>
                <b>Tel. Emerg�ncia</b>
              </label>
              <input type="number" class="form-control" placeholder="(xx)xxxxx-xxxx">
              <br>
              <!-- Campo E-mail pessoal -->
              <label>
                <b>E-mail</b>
              </label>
              <input type="email" class="form-control" placeholder="example@hotmail.com">
              <br>
              <!-- Campo E-mail Stefanini -->
              <label>
                <b>E-mail Stefanini</b>
              </label>
              <input type="email" class="form-control" placeholder="example@latam.stefanini.com">
              <br>
              <!--Bot�o enviar -->
              <button type="submit" class="btn btn-success">Salvar</button>
            </div>
            <br>
            <!-- Dados Academicos -->
            <div class="form-group">
              <h1> Dados Academicos </h1>
              <br>
              <!--Campo nome da faculdade -->
              <label>
                <b>Faculdade</b>
              </label>
              <select class="form-control" id="faculdade">
                <option value="Selecione">Selecione</option>
                <option value="Instituto Federal de S�o Paulo (IFSP)">Instituto Federal de S�o Paulo (IFSP)</option>
                <option value="Funda��o Dom Cabral (FDC)">Funda��o Dom Cabral (FDC)</option>
                <option value="Universidade do ABC (UFABC)">Universidade do ABC (UFABC)</option>
                <option value="Universidade Federal de S�o Paulo (UNIFESP)">Universidade Federal de S�o Paulo (UNIFESP)</option>
                <option value="Universidade Paulista J�lio de Mesquita Filho (UNESP)">Universidade Paulista J�lio de Mesquita Filho (UNESP)</option>
                <option value="Centro Universit�rio Adventista de S�o Paulo (UNASP)">Centro Universit�rio Adventista de S�o Paulo (UNASP)</option>
                <option value="Centro Universit�rio Assun��o (UNIFAI)">Centro Universit�rio Assun��o (UNIFAI)</option>
                <option value="Centro Universit�rio Belas Artes de S�o Paulo (FEBASP)">Centro Universit�rio Belas Artes de S�o Paulo (FEBASP)</option>
                <option value="Centro Universit�rio Capital (UNICAPITAL)">Centro Universit�rio Capital (UNICAPITAL)</option>
                <option value="Centro Universit�rio das Faculdades Metropolitanas Unidas (FMU)">Centro Universit�rio das Faculdades Metropolitanas Unidas (FMU)</option>
                <option value="Centro Universit�rio de Araraquara (UNIARA)">Centro Universit�rio de Araraquara (UNIARA)</option>
                <option value="Centro Universit�rio FECAP">Centro Universit�rio FECAP</option>
                <option value="Centro Universit�rio �talo - Brasileiro (UN�TALO)">Centro Universit�rio �talo - Brasileiro (UN�TALO)</option>
                <option value="Centro Universit�rio Paulistano (UNIPAULISTA)">Centro Universit�rio Paulistano (UNIPAULISTA)</option>
                <option value="Centro Universit�rio Salesiano de S�o Paulo (UNISAL)">Centro Universit�rio Salesiano de S�o Paulo (UNISAL)</option>
                <option value="Centro Universit�rio S�o Camilo">Centro Universit�rio S�o Camilo</option>
                <option value="Centro Universit�rio FIAMFAAM">Centro Universit�rio FIAMFAAM</option>
                <option value="Conservat�rio Dram�tico Musical S�o Paulo (CDMSP)">Conservat�rio Dram�tico Musical S�o Paulo (CDMSP)</option>
                <option value="Escola da Cidade">Escola da Cidade</option>
                <option value="Escola Superior de Engenhar�a e Gest�o de S�o Paulo (ESEG)">Escola Superior de Engenhar�a e Gest�o de S�o Paulo (ESEG)</option>
                <option value="Escola Superior de Propaganda e Marketing (ESPM)">Escola Superior de Propaganda e Marketing (ESPM)</option>
                <option value="Faculdade Albert Einstein de S�o Paulo (FAESP)">Faculdade Albert Einstein de S�o Paulo (FAESP)</option>
                <option value="Faculdade Anglo Latino (FAL)">Faculdade Anglo Latino (FAL)</option>
                <option value="Faculdade Associada Brasil (FAB)">Faculdade Associada Brasil (FAB)</option>
                <option value="Faculdade C�sper L�bero (FCL)">Faculdade C�sper L�bero (FCL)</option>
                <option value="Faculdade Cruz Azul (FACRAZ)">Faculdade Cruz Azul (FACRAZ)</option>
                <option value="Faculdade das Am�ricas (FAM)">Faculdade das Am�ricas (FAM) </option>
                <option value="Faculdade de Administra��o e Ci�ncias Cont�beis Luzwell">Faculdade de Administra��o e Ci�ncias Cont�beis Luzwell</option>
                <option value="Faculdade de Administra��o S�o Paulo (FAPI)">Faculdade de Administra��o S�o Paulo (FAPI)</option>
                <option value="Faculdade de Ci�ncias da Sa�de de S�o Paulo (FCIS)">Faculdade de Ci�ncias da Sa�de de S�o Paulo (FCIS)</option>
                <option value="Faculdade de Ci�ncias M�dicas da Santa Casa S�o Paulo (FCMSCSP)">Faculdade de Ci�ncias M�dicas da Santa Casa S�o Paulo (FCMSCSP)</option>
                <option value="Faculdade de Direito Prof. Dam�sio de Jesus (FDDJ)">Faculdade de Direito Prof. Dam�sio de Jesus (FDDJ)</option>
                <option value="Faculdade de Educa��o Costa Braga (FCB)">Faculdade de Educa��o Costa Braga (FCB)</option>
                <option value="Faculdade de Educa��o e Ci�ncias Gerenciais de S�o Paulo (FECG)">Faculdade de Educa��o e Ci�ncias Gerenciais de S�o Paulo (FECG)</option>
                <option value="Faculdade de Educa��o e Cultura Montessori (FAMEC)">Faculdade de Educa��o e Cultura Montessori (FAMEC)</option>
                <option value="Faculdade de Enfermagem do Hospital Israelita A. Einstein (FEHIAE)">Faculdade de Enfermagem do Hospital Israelita A. Einstein (FEHIAE)</option>
                <option value="Faculdade de Engenharia de S�o Paulo (FESP)">Faculdade de Engenharia de S�o Paulo (FESP)</option>
                <option value="Faculdade de Inform�tica e Administra��o Paulista (FIAP)">Faculdade de Inform�tica e Administra��o Paulista (FIAP)</option>
                <option value="Faculdade de M�sica Carlos Gomez (FMCG)">Faculdade de M�sica Carlos Gomez (FMCG)</option>
                <option value="Faculdade de M�sica Souza Lima (FMSL)">Faculdade de M�sica Souza Lima (FMSL)</option>
                <option value="Faculdade de S�o Bento (FSB)">Faculdade de S�o Bento (FSB)</option>
                <option value="Faculdade de Tecnologia �lvares de Azevedo (FAATESP)">Faculdade de Tecnologia �lvares de Azevedo (FAATESP)</option>
                <option value="Faculdade de Tecnologia Bandeirantes (BANDTEC)">Faculdade de Tecnologia Bandeirantes (BANDTEC)</option>
                <option value="Faculdade de Tecnologia Diamante (FATED)">Faculdade de Tecnologia Diamante (FATED)</option>
                <option value="Fac. de Tec. em Hoteleria Gastro. e Turismo de S�o Paulo (HOTEC)">Fac. de Tec. em Hoteleria Gastro. e Turismo de S�o Paulo (HOTEC)</option>
                <option value="Faculdade de Tecnologia FINACI">Faculdade de Tecnologia FINACI</option>
                <option value="Faculdade de Tecnologia IBTA">Faculdade de Tecnologia IBTA</option>
                <option value="Faculdade de Tecnologia Interam�rica (FTI)">Faculdade de Tecnologia Interam�rica (FTI)</option>
                <option value="Faculdade de Teo. da Igreja Presbiteriana Independiente (FATIPI)">Faculdade de Teo. da Igreja Presbiteriana Independiente (FATIPI)</option>
                <option value="Faculdade do Povo (FAP)">Faculdade do Povo (FAP)</option>
                <option value="Faculdade Evang�lica de S�o Paulo (FAESP)">Faculdade Evang�lica de S�o Paulo (FAESP)</option>
                <option value="Faculdade FIPECAFI">Faculdade FIPECAFI</option>
                <option value="Faculdade Flamingo">Faculdade Flamingo</option>
                <option value="Faculdade Guaian�s (FAG)">Faculdade Guaian�s (FAG)</option>
                <option value="Faculdade Impacta de Tecnologia (FIT)">Faculdade Impacta de Tecnologia (FIT)</option>
                <option value="Faculdade Independente Butant� (FIB)">Faculdade Independente Butant� (FIB)</option>
                <option value="Faculdade Integral Cantareira (FIC)">Faculdade Integral Cantareira (FIC)</option>
                <option value="Faculdade Interlagos de Educa��o e Cultura (FINTEC)">Faculdade Interlagos de Educa��o e Cultura (FINTEC)</option>
                <option value="Faculdade Legale (FALEG)">Faculdade Legale (FALEG)</option>
                <option value="Faculdade CBES">Faculdade CBES</option>
                <option value="Faculdade Mario de Andrade (FMA)">Faculdade Mario de Andrade (FMA)</option>
                <option value="Faculdade Meassi�nica">Faculdade Meassi�nica</option>
                <option value="Faculdade M�todo de S�o Paulo (FAMESP)">Faculdade M�todo de S�o Paulo (FAMESP)</option>
                <option value="Faculdade M�dulo">Faculdade M�dulo</option>
                <option value="Faculdade Morumbi Sul (FMS)">Faculdade Morumbi Sul (FMS)</option>
                <option value="Faculdade Mozarteum de S�o Paulo (FAMOSP)">Faculdade Mozarteum de S�o Paulo (FAMOSP)</option>
                <option value="Faculdade Mundial">Faculdade Mundial</option>
                <option value="Faculdade Paulista de Artes (FPA)">Faculdade Paulista de Artes (FPA)</option>
                <option value="Faculdade Paulista Ci�ncias Aplicadas (FPCA)">Faculdade Paulista Ci�ncias Aplicadas (FPCA)</option>
                <option value="Faculdade Paulista de Servi�o Social (FAPSS)">Faculdade Paulista de Servi�o Social (FAPSS)</option>
                <option value="Faculdade Paulus de Tecnologia e Comunica��o (FAPCOM)">Faculdade Paulus de Tecnologia e Comunica��o (FAPCOM)</option>
                <option value="Faculdade Pr�xis (FIPEP)">Faculdade Pr�xis (FIPEP)</option>
                <option value="Faculdade Santa Marcelina (FASM)">Faculdade Santa Marcelina (FASM)</option>
                <option value="Faculdade Santa Rita">Faculdade Santa Rita </option>
                <option value="Faculdade S�o Paulo (FACSP)">Faculdade S�o Paulo (FACSP)</option>
                <option value="Faculdade Sequencial">Faculdade Sequencial</option>
                <option value="Faculdade Sumar�">Faculdade Sumar�</option>
                <option value="Faculdade Tancredo Neves (FTN)">Faculdade Tancredo Neves (FTN)</option>
                <option value="Faculdade Teol�gica Batista de S�o Paulo (FTBSP)">Faculdade Teol�gica Batista de S�o Paulo (FTBSP)</option>
                <option value="Faculdade Teol�gica de Ci�ncias Humanas e Sociais Logos (FAETEL)">Faculdade Teol�gica de Ci�ncias Humanas e Sociais Logos (FAETEL)</option>
                <option value="Faculdade Uni�o (FTBSP)">Faculdade Uni�o (FTBSP)</option>
                <option value="Faculdade Zumbi dos Palmares (FAZP)">Faculdade Zumbi dos Palmares (FAZP)</option>
                <option value="Faculdade Reuninda (FAR)">Faculdade Reuninda (FAR)</option>
                <option value="Faculdades Associadas de S�o Paulo (FASP)">Faculdades Associadas de S�o Paulo (FASP)</option>
                <option value="Faculdades Integradas de S�o Paulo (FISP)">Faculdades Integradas de S�o Paulo (FISP)</option>
                <option value="Faculdades Integradas Paulista (FIP)">Faculdades Integradas Paulista (FIP)</option>
                <option value="Faculdades Integradas Rio Branco (FRB)">Faculdades Integradas Rio Branco (FRB)</option>
                <option value="Faculdades Integradas Tibiri�� (FATI)">Faculdades Integradas Tibiri�� (FATI)</option>
                <option value="Faculdades Oswaldo Cruz (FOC)">Faculdades Oswaldo Cruz (FOC)</option>
                <option value="Funda��o Armando Alvares Penteado (FAAP)">Funda��o Armando Alvares Penteado (FAAP) </option>
                <option value="Funda��o Escola de Sociolog�a e Pol�tica de S�o Paulo (FESPSP)">Funda��o Escola de Sociolog�a e Pol�tica de S�o Paulo (FESPSP)</option>
                <option value="Funda��o Getulio Vargas (FGV)">Funda��o Getulio Vargas (FGV)</option>
                <option value="Funda��o Instituto de Administra��o (FIA)">Funda��o Instituto de Administra��o (FIA)</option>
                <option value="Insituto de Ensino e Pesquisa (INSPER)">Insituto de Ensino e Pesquisa (INSPER)</option>
                <option value="Instituto Europeo de Desing (IED)">Instituto Europeo de Desing (IED)</option>
                <option value="Instituto Nacional de P�s-Gradua��o (INPG)">Instituto Nacional de P�s-Gradua��o (INPG)</option>
                <option value="Instituto S�o Paulo de Enstudos Superiores (ITESP)">Instituto S�o Paulo de Enstudos Superiores (ITESP)</option>
                <option value="Instituto Superior de Educa��o Alborada Plus (ISEAP)">Instituto Superior de Educa��o Alborada Plus (ISEAP)</option>
                <option value="Instituto Superior de Educa��o de S�o Paulo (SINGULARIDADES-ISESP)">Instituto Superior de Educa��o de S�o Paulo (SINGULARIDADES-ISESP)</option>
                <option value="Instituto Superior de Educa��o Vera Cruz (ISEVC)">Instituto Superior de Educa��o Vera Cruz (ISEVC)</option>
                <option value="Panamericana Escola de Arte e Design">Panamericana Escola de Arte e Design</option>
                <option value="Pontif�cia Universidade Cat�lica de S�o Paulo (PUCSP)">Pontif�cia Universidade Cat�lica de S�o Paulo (PUCSP)</option>
                <option value="SENAI - Servico Nacional de Aprendizagem Industrial">SENAI - Servico Nacional de Aprendizagem Industrial</option>
                <option value="Trevisan Escola de Negocios">Trevisan Escola de Negocios</option>
                <option value="Universidade Anhembi Morumbi">Universidade Anhembi Morumbi</option>
                <option value="Universidade Braz Cubas (UBC)">Universidade Braz Cubas (UBC)</option>
                <option value="Universidade Bandeirantes de S�o Paulo (UNIBAN)">Universidade Bandeirantes de S�o Paulo (UNIBAN)</option>
                <option value="Universidade Camilo Castelo Branco (UNICASTELO)">Universidade Camilo Castelo Branco (UNICASTELO)</option>
                <option value="Universidade Cidade de S�o Paulo (UNICID)">Universidade Cidade de S�o Paulo (UNICID)</option>
                <option value="Universidade Cruzeiro do Sul">Universidade Cruzeiro do Sul</option>
                <option value="Universidade de Guarulhos (UNG)">Universidade de Guarulhos (UNG)</option>
                <option value="Universidade de Santo Amaro (UNISA)">Universidade de Santo Amaro (UNISA)</option>
                <option value="Universidade de S�o Paulo (USP)">Universidade de S�o Paulo (USP)</option>
                <option value="Universidade do Oeste Paulista (UNOESTE)">Universidade do Oeste Paulista (UNOESTE)</option>
                <option value="Universidade Ibirapuera (UNIB)">Universidade Ibirapuera (UNIB)</option>
                <option value="Universidade Mackenzie">Universidade Mackenzie</option>
                <option value="Universidade Metodista de S�o Paulo">Universidade Metodista de S�o Paulo</option>
                <option value="Universidade Nove de Julho (UNINOVE)">Universidade Nove de Julho (UNINOVE)</option>
                <option value="Universidade Paulista (UNIP)">Universidade Paulista (UNIP)</option>
                <option value="Universidade S�o Francisco (USF)">Universidade S�o Francisco (USF)</option>
                <option value="Universidade S�o Marcos">Universidade S�o Marcos</option>
                <option value="Universidade S�o Judas Tadeu (USJT)">Universidade S�o Judas Tadeu (USJT)</option>
                <option value="Instituto Butantan">Instituto Butantan</option>
                <option value="Faculdades Integradas Campos Salles (FICS)">Faculdades Integradas Campos Salles (FICS)</option>
                <option value="Instituto Bandeirante de Educa��o e Cultura (IBEC)">Instituto Bandeirante de Educa��o e Cultura (IBEC)</option>
                <option value="Escola Superior Aberta do Brasil (ESAB)">Escola Superior Aberta do Brasil (ESAB)</option>
                <option value="Instituto de P�s-Gradua��o &amp; Gradua��o (IPOG)">Instituto de P�s-Gradua��o &amp; Gradua��o (IPOG)</option>
                <option value="Escola Superior de Negocios Trevisan">Escola Superior de Negocios Trevisan</option>
                <option value="Outros">Outros</option>
              </select>
              <br>
              <!--Campo de sele��o do curso-->
              <div class="form-group">
                <label>
                  <b>Curso</b>
                </label>
                <select class="form-control" id="curso">
                  <option value="Selecione">Selecione</option>
                  <option value="Com�cio Exterior">Com�cio Exterior</option>
                  <option value="Gest�o Comercial">Gest�o Comercial</option>
                  <option value="Gest�o de Recursos Humanos">Gest�o de Recursos Humanos</option>
                  <option value="Gest�o Financeira">Gest�o Financeira</option>
                  <option value="Gest�o Publica">Gest�o Publica</option>
                  <option value="Log�stica">Log�stica</option>
                  <option value="Marketing">Marketing</option>
                  <option value="Processos Gerenciais">Processos Gerenciais</option>
                  <option value="Design de Interiores">Design de Interiores</option>
                  <option value="Design de Moda">Design de Moda</option>
                  <option value="Design Gr�fico">Design Gr�fico</option>
                  <option value="Fotografia">Fotografia</option>
                  <option value="Produ��o Audiovisual">Produ��o Audiovisual</option>
                  <option value="Produ��o Multim�dia">Produ��o Multim�dia</option>
                  <option value="Produ��o Publicit�ria">Produ��o Publicit�ria</option>
                  <option value="Publicidade e Propaganda">Publicidade e Propaganda</option>
                  <option value="An�lise e Desenvolvimento de Sistemas">An�lise e Desenvolvimento de Sistemas</option>
                  <option value="Banco de Dados">Banco de Dados</option>
                  <option value="Gest�o da Tecnologia da Informa��o">Gest�o da Tecnologia da Informa��o</option>
                  <option value="Jogos Digitais">Jogos Digitais</option>
                  <option value="Redes de Computadores">Redes de Computadores</option>
                  <option value="Seguran�a da Informa��o">Seguran�a da Informa��o</option>
                  <option value="Sistemas para Internet">Sistemas para Internet</option>
                  <option value="Biomedicina">Biomedicina</option>
                  <option value="Ci�ncias Biologicas">Ci�ncias Biologicas</option>
                  <option value="Educa��o">Educa��o F�sica</option>
                  <option value="Enfermagem">Enfermagem</option>
                  <option value="Farm�cia">Farm�cia</option>
                  <option value="Fisioterapia">Fisioterapia</option>
                  <option value="Fonoaudiologia">Fonoaudiologia</option>
                  <option value="Medicina">Medicina Verterin�ria</option>
                  <option value="Nutri��o">Nutri��o</option>
                  <option value="Ondotologia">Ondotologia</option>
                  <option value="Arquitetura e Urbanismo">Arquitetura e Urbanismo</option>
                  <option value="Desenho Industrial">Desenho Industrial</option>
                  <option value="Engenharia Ambiental">Engenharia Ambiental</option>
                  <option value="Engenharia Aeron�utica">Engenharia Aeron�utica</option>
                  <option value="Engenharia de Computa��o">Engenharia de Computa��o</option>
                  <option value="Engenharia de Petr�leo">Engenharia de Petr�leo</option>
                  <option value="Engenharia de Produ��o">Engenharia de Produ��o</option>
                  <option value="Engenharia de Produ��o Mec�nica">Engenharia de Produ��o Mec�nica</option>
                  <option value="Engenharia El�trica-Eletr�nica">Engenharia El�trica-Eletr�nica</option>
                  <option value="F�sica">F�sica</option>
                  <option value="Matem�tica">Matem�tica</option>
                  <option value="Sistemas de Informa��o">Sistemas de Informa��o</option>
                  <option value="Administra��o">Administra��o</option>
                  <option value="Ci�ncias Atuariais">Ci�ncias Atuariais</option>
                  <option value="Ci�ncias Cont�beis">Ci�ncias Cont�beis</option>
                  <option value="Ci�ncias Econ�micas">Ci�ncias Econ�micas</option>
                  <option value="Comunica��o">Comunica��o Social</option>
                  <option value="Direito">Direito</option>
                  <option value="Geografia">Geografia</option>
                  <option value="Hist�ria">Hist�ria</option>
                  <option value="Hotelaria">Hotelaria</option>
                  <option value="Jornalismo">Jornalismo</option>
                  <option value="Letras Licenciatura em Lingua Portuguesa">Letras Licenciatura em Lingua Portuguesa</option>
                  <option value="Letras Licenciatura em Lingua Portuguesa e Lingua Inglesa">Letras Licenciatura em Lingua Portuguesa e Lingua Inglesa</option>
                  <option value="Letras Licenciatura em Lingua Portuguesa e Lingua Espanhola">Letras Licenciatura em Lingua Portuguesa e Lingua Espanhola</option>
                  <option value="Moda">Moda</option>
                  <option value="Pedagogia">Pedagogia</option>
                  <option value="Propaganda e Marketing">Propaganda e Marketing</option>
                  <option value="Psicologia">Psicologia</option>
                  <option value="Rela��es Internacionais">Rela��es Internacionais</option>
                  <option value="Secretariado Execultivo">Secretariado Execultivo</option>
                  <option value="Servi�o Socia">Servi�o Social</option>
                  <option value="Turismo">Turismo</option>
                  <option value="Outros">Outros</option>
                </select>
                <br>
                <!-- Campo de dura��o do curso-->
                <label>
                  <b>Dura��o do Curso</b>
                </label>
                <input type="text" class="form-control" placeholder="Informe em Semestre">
                <br>
                <!-- Selec�o de semestre -->
                <label>
                  <b>Semestre Atual</b>
                </label>
                <input type="text" class="form-control" placeholder="Semestre Atual">
                <br>
                <!--Sele��o de data de in�cio do curso-->
                <label>
                  <b>Data de In�cio do Curso</b>
                </label>
                <input type="date" class="form-control">
                <br>
                <!-- Termino do curso -->
                <label>
                  <b>T�rmino do Curso</b>
                </label>
                <input type="date" class="form-control">
                <br>
                <!-- Bot�o salvar -->
                <button type="submit" class="btn btn-success">Salvar</button>
              </div>
            </div>
            <br>
            <br>
            <!-- Redefini��o de senha -->
            <div class="form-group">
              <h1> Redefini��o de Senha </h1>
              <label>Senha Atual</label>
              <input type="password" class="form-control" placeholder="">
              <label>Nova Senha</label>
              <input type="password" class="form-control" placeholder="">
              <label>Confirmar Senha</label>
              <input type="password" class="form-control" placeholder="">
              <br>
              <button type="submit" class="btn btn-success">Salvar</button>
              <br>
              <br> </div>
            <br>
            <br> </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <div class="text-white py-0 bg-secondary">
    <div class="container">
      <div class="row">
        <div class="col-md-12 mt-3 text-center">
          <p>� Copyright 2018 Stefanini IT Solutions &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <a href="https://stefanini.com/en/privacy-policy/">Privacy Politicy</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</body>

</html>