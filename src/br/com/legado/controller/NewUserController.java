package br.com.legado.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class NewUserController
 */
@WebServlet("/user/new")
public class NewUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	 
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String birth = request.getParameter("txtBirth");
		Date data = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("DD/MM/yyyy");
		try {
			data = sdf.parse(birth);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println(sdf.format(data));
	}

}
