package br.com.legado.security.model;

import javax.persistence.EntityManager;

import br.com.legado.config.ConnectionFactory;
import br.com.legado.security.entity.Skill;

public class SkillService {
	
	public static Skill save(Skill skill) {
		EntityManager em = new ConnectionFactory().getConnection();
		try {
			em.getTransaction().begin();
			if (skill.getId() == null) {
				em.persist(skill);
			} else {
				em.merge(skill);
			}
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.err.println(e.getMessage());
		} finally {
			em.close();
		}
		return skill;
	}

}
