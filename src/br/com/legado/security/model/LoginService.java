package br.com.legado.security.model;

import javax.persistence.EntityManager;

import br.com.legado.config.ConnectionFactory;
import br.com.legado.security.entity.Login;

public class LoginService {
	
	public static Login save(Login login) {
		EntityManager em = new ConnectionFactory().getConnection();
		try {
			em.getTransaction().begin();
			if (login.getId() == null) {
				em.persist(login);
			} else {
				em.merge(login);
			}
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return login;
	}
	
	public static Login findById(Long id) {
		EntityManager em = new ConnectionFactory().getConnection();
		Login login = em.find(Login.class, id);
		try {
			login.setId(login.getId());
			login.setEmail(login.getEmail());
			login.setRole(login.getRole());
			
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}finally {
			em.close();
		}
		return login;
	}
	
	public static Boolean remove(Long id) {
		EntityManager em = new ConnectionFactory().getConnection();
		Login login = em.find(Login.class, id);
		try {
		em.getTransaction().begin();
		em.remove(login);
		em.getTransaction().commit();
		}catch(Exception e) {
			em.getTransaction().rollback();
			System.err.println(e.getMessage());
			em.close();
			return false;
		}finally {
			em.close();
		}
		return true;
	}
	
	public static Boolean log(Long id) {
		EntityManager em = new ConnectionFactory().getConnection();
		Login old = LoginService.findById(id);
		return true;
	}
}
