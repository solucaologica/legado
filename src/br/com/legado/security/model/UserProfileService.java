package br.com.legado.security.model;

import javax.persistence.EntityManager;

import br.com.legado.config.ConnectionFactory;
import br.com.legado.security.entity.UserProfile;

public class UserProfileService {
	
	public static UserProfile save(UserProfile user) {
		EntityManager em = new ConnectionFactory().getConnection();
		try {
			em.getTransaction().begin();
			if (user.getId() == null) {
				em.persist(user);
			} else {
				em.merge(user);
			}
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.err.println(e.getMessage());
		} finally {
			em.close();
		}
		return user;
	}
	
}
