package br.com.legado.security.model;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Query;

import br.com.legado.config.ConnectionFactory;
import br.com.legado.security.entity.TokenRequest;

public class TokenRequestService {

	public TokenRequest save(TokenRequest token) {
		EntityManager em = new ConnectionFactory().getConnection();
		try {
			em.getTransaction().begin();
			if (token.getId() == null) {
				em.persist(token);
			} else {
				em.merge(token);
			}
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return token;
	}

	public static Boolean validateEmail(String email) {
		return ((!email.isEmpty()) && email.length() > 14);
	}

	public String generateToken() {
		return "xoxo";
	}


//	public TokenRequestBean findByEmail(String email) {
//		EntityManager em = new ConnectionFactory().getConnection();
//		TokenRequestBean token = (TokenRequestBean) em.createQuery(
//				"select t "
//				+ "from tokenrequestbean t "
//				+ "where t.email like :email")
//				.setParameter("email", email)
//				.getSingleResult();
//		return token;
//	}
}
